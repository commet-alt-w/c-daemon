#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <pthread.h>
#include <syslog.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/socket.h>
#include <sys/un.h>

// set pid file
#define PIDFILE "/var/tmp/c-daemon.pid"
// set io flags
#define PIDFLAGS (O_RDWR|O_CREAT)
// set r/w permissions
// user r/w, group read, other read, group id
#define PIDMODE (S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH|S_ISGID)
// set socket file
#define SOCKETFILE "/var/tmp/c-daemon.sock"
// socket input buffer size
#define SOCKBUFFSIZE 2

// thread info struct
struct thread_info {   
  pthread_t  thread_id;
  int        thread_num;
  sigset_t   thread_mask;
  char      *argv_string;
};

// signal handler args
struct sigh_args {
  struct thread_info *t_info;
};

// socket handler args
struct sockh_args {
  struct thread_info *t_info;
  int sockfd;
  int data_sockfd;
};

// daemon args
struct daemon_args {
  struct sigh_args *siga;
  struct sockh_args *socka;
  int pidfile_fd;
};

/*
 * delete_socket_file
 *
 * delete socket file with unlink
 */
void delete_socket_file() {
  // use unlink to delete socket file
  unlink(SOCKETFILE);
}

/*
 * set_thread_cancel_state_type
 *
 * set cancel state/type for calling thread
 * return -1 if it fails
 * return 0 on success
 */
int set_thread_cancel_state_type(int state, int type) {
  // set cancel state
  if(pthread_setcancelstate(state, NULL) != 0) {
    syslog(LOG_ERR, "can't set cancel state for thread");
    return -1;
  }

  // set cancel type
  if (pthread_setcanceltype(type, NULL) != 0) {
    syslog(LOG_ERR, "can't set cancel type for thread");
    return -1;
  }

  return 0;
}

/*
 * daemon_cleanup
 *
 * close open file descriptors
 * free memory
 * close log
 */
void daemon_cleanup(struct daemon_args *dargs) {
  // close pidfile fd
  close(dargs->pidfile_fd);

  // close socket file descriptors
  close(dargs->socka->sockfd);
  if (dargs->socka->data_sockfd > 0) {
    close(dargs->socka->data_sockfd);
  }

  // free memory
  /* thread info */
  free(dargs->siga->t_info);
  free(dargs->socka->t_info);
  /* signal handler args */
  free(dargs->siga);
  /* socket handler args */
  free(dargs->socka);
  /* daemon args */
  free(dargs);

  // close log
  closelog();

  // delete socket file
  delete_socket_file();
}

/*
 * use pid file to prevent more daemons from running
 *
 * return -1 if it fails
 * return pidfile file descriptor on success
 */
int lock_daemon() {
  // apply or remove adisory lock on file
  struct flock fl;
  //  try to open or create pid file
  int fd = open(PIDFILE, PIDFLAGS, PIDMODE);

  // check for io errors
  if (fd < 0) {
    syslog(LOG_ERR, "can't create or open pid file: %s", PIDFILE);
    switch(errno) {
    case EACCES:
      syslog(LOG_ERR, "ERROR: %s", strerror(errno));
      return -1;
    default:
      syslog(LOG_ERR, "ERROR: %s", strerror(errno));
      return -1;
    }
  }

  // setup file lock
  // write lock
  fl.l_type = F_WRLCK;
  // starting offset (in bytes) for write lock
  fl.l_start = 0;
  // set interpretation of l_start
  // to start beginning of file
  fl.l_whence = SEEK_SET;
  // lock all bytes specified by
  // whence and start to the end of the file
  fl.l_len = 0;
  // write lock the file descriptor
  int fcntlr = fcntl(fd, F_SETLK, &fl);

  // check for errors
  if (fcntlr < 0) {
    syslog(LOG_ERR, "cannot lock pid file: %s", PIDFILE);
    switch(errno) {
    case EACCES:
      syslog(LOG_ERR, "ERROR: %s", strerror(errno));
      close(fd);
      return -1;
    case EAGAIN:
      syslog(LOG_ERR, "ERROR: %s", strerror(errno));
      close(fd);
      return -1;
    default:
      syslog(LOG_ERR, "ERROR: %s", strerror(errno));
      close(fd);
      return -1;
    }
  }

  // write process pid to pid file
  pid_t pid = getpid();
  int len = snprintf(NULL, 0, "%ld", (long)pid);
  char * const buffer = calloc(len+1, sizeof(char));
  if (snprintf(buffer, len+1, "%ld", (long)pid) > 0) {
    write(fd, buffer, len+1);
  }
  free(buffer);

  // return file descriptor
  return fd;
}

/*
 * daemonize
 *
 * make the running process a daemon
 * using double fork and setsid
 */
void daemonize() {
  // signal result, chdir result
  int sigr, chdr;
  // process pid
  pid_t pid;
  // signal action handling
  struct sigaction sa;

  // clear create file mask
  umask(0);

  /* 
     process is not session leader
     process is group leader
  */

  // fork from parent
  pid = fork();
  if (pid < 0) {
    syslog(LOG_ERR, "can't fork");
    exit(EXIT_FAILURE);
  } else if (pid != 0) {
    // success, let parent exit
    exit(EXIT_SUCCESS);
  }

  /*
    child process continues after first fork
   */
  
  // set session leader
  setsid();

  /* 
     child process is session leader
     child process is group leader
  */

  // set handler to ignore signals
  sa.sa_handler = SIG_IGN;
  // initialize signal set to empty
  // and exclude signals (set to block all signals)
  sigemptyset(&sa.sa_mask);
  // no flag
  sa.sa_flags = 0;

  // ignore sighup up before second fork
  // sighup = "hang up"
  //  i.e. originating terminal connection terminated
  sigr = sigaction(SIGHUP, &sa, NULL);
  if (sigr < 0) {
    syslog(LOG_ERR, "can't ignore sighup");
    exit(EXIT_FAILURE);
  }

  // fork again to prevent aquiring controlling terminal
  pid = fork();
  if (pid < 0) {
    syslog(LOG_ERR, "can't fork");
    exit(EXIT_FAILURE);
  } else if (pid != 0) {
    // success, let parent exit
    exit(EXIT_SUCCESS);
  }

  /*
    child process is not session leader
    child process is not group leader
  */

  //change cwd to root
  chdr = chdir("/");
  if (chdr < 0) {
    syslog(LOG_ERR, "can't change cwd to root");
    exit(EXIT_FAILURE);
  }

  // connect stdin, stdout, stderr to /dev/null
  int fd0 = open("/dev/null", O_RDONLY, 0666);
  int fd1 = open("/dev/null", O_WRONLY, 0666);
  int fd2 = open("/dev/null", O_RDWR, 0666);

  dup2(fd0, 0);
  dup2(fd1, 1);
  dup2(fd2, 2);

  close(fd0);
  close(fd1);
  close(fd2);
}

/*
 * signal_handler(struct thread_info *arg)
 * 
 * process signals
 * return thread_id if it ever exits
 */
void *signal_handler(void *args) {
   // set thread cancel state/type
  set_thread_cancel_state_type(PTHREAD_CANCEL_ENABLE,
                               PTHREAD_CANCEL_ASYNCHRONOUS);

  // cast void arg
  struct daemon_args * const dargs = args;
  struct thread_info * const t_info = dargs->siga->t_info;

  // init signal
  int signal = 0;

  // print thread info
  syslog(LOG_INFO, "thread_id: %d", t_info->thread_id);
  syslog(LOG_INFO, "thread_num: %d", t_info->thread_num);
  syslog(LOG_INFO, "thread_mask: %d", t_info->thread_mask);
  syslog(LOG_INFO, "argv_string: %s", t_info->argv_string);

  // signal handler loop
  while (true) {
    syslog(LOG_INFO, "waiting for signal");

    // wait for signal
    int sigr = sigwait(&t_info->thread_mask, &signal);
    if (sigr != 0) {
      syslog(LOG_ERR, "sigwait failed");
      // cancel socket handler thread
      pthread_cancel(dargs->socka->t_info->thread_id);
      daemon_cleanup(dargs);
      exit(EXIT_FAILURE);
    }

    // log signal
    syslog(LOG_INFO, "signal: %d", signal);

    // process signals
    switch (signal) {
    case SIGHUP: // 1
      syslog(LOG_INFO, "sighup recieved: not gonna do anything about it");
      break;
    case SIGTERM: // 15
      syslog(LOG_INFO, "sigterm recieved: exiting");
      // cancel socket handler thread
      pthread_cancel(dargs->socka->t_info->thread_id);
      daemon_cleanup(dargs);
      exit(EXIT_SUCCESS);
      break;
    case SIGKILL: // 9
      syslog(LOG_ERR, "sigkill is never recieved, handled by os/kernel");
      daemon_cleanup(dargs);
      exit(EXIT_FAILURE);
      break;
    default: // ?
      syslog(LOG_ERR, "signal unaccounted for: %d", signal);
      break;
    }
  }

  return t_info->thread_id;
}

/*
 * create signal handler thread
 * return 0 on success
 * otherwise log error and return pthread result
 */
int create_signal_handler_thread(struct daemon_args *dargs) {
  int pthreadr = 0;
  struct thread_info * const t_info = dargs->siga->t_info;

  // block default signal handling for thread
  pthreadr = pthread_sigmask(SIG_BLOCK, &t_info->thread_mask, NULL);
  if (pthreadr != 0) {
    syslog(LOG_ERR, "can't sig_block error");
    return pthreadr;
  }

  // create signal handler thread
  pthreadr = pthread_create(&t_info->thread_id, NULL, signal_handler, dargs);
  if (pthreadr != 0) {
    syslog(LOG_ERR, "can't create signal handler thread");
    return pthreadr;
  }

  return pthreadr;
}

/*
 * start_signal_handler_thread
 *
 * return 0 on success
 * return -1 otherwise
 */
int start_signal_handler_thread(struct daemon_args *dargs) {
  // define how to interact with signals
  struct sigaction sa;

  // set default signal handler
  sa.sa_handler = SIG_DFL;
  // empty signal set
  // exclude signals (set to block all signals)
  sigemptyset(&sa.sa_mask);
  // no flags
  sa.sa_flags = 0;

  // reset default signal handler for sighup to default
  // after daemonize changed it to SIG_IGN
  //
  // block all signals
  if (sigaction(SIGHUP, &sa, NULL) < 0) {
    syslog(LOG_ERR, "can't ignore sighup");
    return -1;
  }

  // initialize signal set to full
  // include all signals
  sigfillset(&dargs->siga->t_info->thread_mask);

  // create signal handler thread
  dargs->siga->t_info->thread_num = 1;

  if (create_signal_handler_thread(dargs) != 0) {
    syslog(LOG_ERR, "error creating signal handler thread");
    return -1;
  }

  return 0;
}

void *socket_handler(void *args) {
  // cast void arg
  struct daemon_args * const dargs = args;
  // initialize data socket to negative value
  // so daemon_cleanup won't try to close it
  // if it never gets set
  dargs->socka->data_sockfd = -1;

  // process connections
  while (true) {
    syslog(LOG_INFO, "socket waiting for connection");
    // wait for connection
    int data_sockfd = accept(dargs->socka->sockfd, NULL, NULL);
    // store data socket file descriptor
    dargs->socka->data_sockfd = data_sockfd;
    // create buffer
    char buffer[SOCKBUFFSIZE];
    memset(&buffer, 0, SOCKBUFFSIZE);

    syslog(LOG_INFO, "socket got connection");

    // check for errors
    if (data_sockfd < 0) {
      syslog(LOG_ERR, "cannot accept socket connections: %s", strerror(errno));
      // cancel signal handler thread
      pthread_cancel(dargs->siga->t_info->thread_id);
      daemon_cleanup(dargs);
      exit(EXIT_FAILURE);
    }

    // process connection options
    while (true) {
      // read data
      if (read(data_sockfd, buffer, SOCKBUFFSIZE) < 0) {
        syslog(LOG_ERR, "cannot read socket data: %s", strerror(errno));
        // cancel signal handler thread
        pthread_cancel(dargs->siga->t_info->thread_id);
        daemon_cleanup(dargs);
        exit(EXIT_FAILURE);
      }

      // make sure buffer is 0 terminated
      buffer[sizeof(buffer) - 1] = 0;

      if (!strncmp(buffer, "b", sizeof(buffer))) {
        // log data
        syslog(LOG_INFO, "received data: %s", buffer);
        break;
      }

      if (!strncmp(buffer, "q", sizeof(buffer))) {
        syslog(LOG_INFO, "received data: %s", buffer);
        syslog(LOG_INFO, "exiting");
        // cancel signal handler thread
        pthread_cancel(dargs->siga->t_info->thread_id);
        daemon_cleanup(dargs);
        exit(EXIT_FAILURE);
      }
     }
    // close data socket
    close(data_sockfd);
  }

  return dargs;
}

/*
 * start socket handler thread
 *
 * return pthread result on success
 * return -1 on failure
 */
int start_socket_handler_thread(const struct daemon_args * const dargs) {
  int pthreadr = 0;

  // set thread number
  // todo: need real thread handling
  dargs->socka->t_info->thread_num = 2;

  // create socket listen thread
  pthreadr = pthread_create(&dargs->socka->t_info->thread_id,
                            NULL,
                            socket_handler,
                            (void *)dargs);

  return pthreadr;
}

/*
 * start_client_socket_interface
 *
 * return -1 if it fails
 * return socket file descriptor otherwise
 */
int start_socket_interface(struct daemon_args * const dargs) {
  // create socket
  int sockfd = socket(AF_UNIX, SOCK_SEQPACKET, 0);
  // check for error
  if (sockfd < 0) {
    syslog(LOG_ERR, "can't create socket: %s", strerror(errno));
    return -1;
  }

  // create unix socket address
  struct sockaddr_un saddr;
  // clear struct before use
  memset(&saddr, 0, sizeof(struct sockaddr_un));
  // set to unix interprocess communication
  saddr.sun_family = AF_UNIX;
  // copy socket file path
  strncpy(saddr.sun_path,
          SOCKETFILE,
          sizeof(saddr.sun_path) - 1);

  // bind address to socket
  int ret = bind(sockfd,
                 (const struct sockaddr *)&saddr,
                 sizeof(struct sockaddr_un));

  // check for error
  if (ret < 0) {
    syslog(LOG_ERR, "cannot bind socket: %s", strerror(errno));
    return -1;
  }

  // set socket to listen with 5 request backlog
  ret = listen(sockfd, 5);
  if (ret < 0) {
    syslog(LOG_ERR, "cannot set socket to listen: %s", strerror(errno));
    return -1;
  }

  // start socket thread
  dargs->socka->sockfd = sockfd;
  if (start_socket_handler_thread(dargs) < 0) {
    syslog(LOG_ERR, "cannot start socket thread: %s", strerror(errno));
    return -1;
  }

  return sockfd;
}

/*
 * start_threads
 *
 * prepare daemon args
 * start signal handler thread
 * start socket interface thread
 */
void start_threads(struct daemon_args *dargs, char *argc) {
  // allocate memory for signal handler args
  dargs->siga = malloc(sizeof(struct sigh_args));
  dargs->siga->t_info = malloc(sizeof(struct thread_info));
  // allocate memory for socket handler args
  dargs->socka = malloc(sizeof(struct sockh_args));
  dargs->socka->t_info = malloc(sizeof(struct thread_info));

  // set sigh_args thread_info argv_string
  dargs->siga->t_info->argv_string = argc;

  // start signal handler thread
  if (start_signal_handler_thread(dargs) != 0) {
    syslog(LOG_ERR, "can't start signal handler thread");
    daemon_cleanup(dargs);
    exit(EXIT_FAILURE);
  }

  // start socket interface for client
  if (start_socket_interface(dargs) < 0) {
    syslog(LOG_ERR, "can't start client socket interface");
    daemon_cleanup(dargs);
    exit(EXIT_FAILURE);
  }
}

/*
 * usage: ./c-daemon qwer
 */
int main(int argv, char **argc) {
  // log args
  syslog(LOG_INFO, "argv: %d", argv);

  for (int i = 0; i < argv; i++) {
    syslog(LOG_INFO, "argc[%d]: %s", i, argc[i]);
  }

  // daemonize
  // pass at least one arg, anything
  // also make sure passed arg is not empty
  if (argv > 1 && strncmp(argc[1], "", sizeof(char)*1) != 0) {
    daemonize();
  } else {
    syslog(LOG_ERR, "need arg: exiting");
    exit(EXIT_FAILURE);
  }

  // initialize log file
  openlog("c-daemon", LOG_CONS, LOG_DAEMON);

  // lock daemon
  int pidfile_fd = lock_daemon();
  if (pidfile_fd < 0) {
    syslog(LOG_ERR, "cannot lock daemon process");
    syslog(LOG_ERR, "is a deamon process already running?");
    closelog();
    exit(EXIT_FAILURE);
  }

  // allocate (heap) memory for daemon args
  struct daemon_args *dargs = malloc(sizeof(struct daemon_args));

  // store daemon pidfile file descriptor
  dargs->pidfile_fd = pidfile_fd;

  // start threads
  start_threads(dargs, argc[1]);

  // run daemon 
  while (true) {
    syslog(LOG_INFO, "c-daemon running");
    sleep(5);
  }

  exit(EXIT_SUCCESS);
}
