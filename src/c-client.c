#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>

#define SOCKETFILE "/var/tmp/c-daemon.sock"
#define SOCKBUFFSIZE 2

/*
 * print_help
 *
 * print help to stdout
 */
void print_help() {
  printf("c-client\n");
  printf("\tusage: c-client -b\n");
}

/*
 * send_data
 *
 * send data to socket
 *
 * return sockfd on success
 * return -1 on failure
 */
int send_data(char *data) {
  char buffer[SOCKBUFFSIZE];
  struct sockaddr_un addr;
  int sockfd;

  // create socket
  sockfd = socket(AF_UNIX, SOCK_SEQPACKET, 0);
  if (sockfd < 0) {
    perror("ERROR: can't create socket");
    close(sockfd);
    exit(EXIT_FAILURE);
  }

  // empty addr and buffer
  memset(&addr, 0, sizeof(addr));
  memset(&buffer, 0, sizeof(buffer));

  // prepare buffer
  strncpy(buffer, data, SOCKBUFFSIZE);

  // set up socket address
  addr.sun_family = AF_UNIX;
  strncpy(addr.sun_path, SOCKETFILE, sizeof(addr.sun_path));

  // connect to socket
  if (connect(sockfd,
              (const struct sockaddr *)&addr,
              sizeof(addr)) < 0) {
    perror("ERROR: can't connect to daemon: ");
    close(sockfd);
    exit(EXIT_FAILURE);
  }

  // write data
  if (write(sockfd, buffer, sizeof(buffer)) < 0) {
    perror("ERROR: can't send data to socket: ");
    return -1;
  }

  close(sockfd);

  return sockfd;
}

/*
 * process_args
 *
 * process command line arguments
 *
 * return 0 on success
 * return -1 otherwise
 */
int process_args(int argc, char *argv[]) {
  // check if at least one argument was passed
  if (argc < 2) {
    printf("ERROR: missing arguments\n");
    print_help();
    return -1;
  }

  int opt = 0;
  while ((opt = getopt(argc, argv, "hbq")) != -1) {
    switch (opt) {
    case 'h':
      print_help();
      break;
    case 'b':
      // send data
      if (send_data("b") < 0) {
        printf("ERROR: can't send data");
        return -1;
      }
      break;
    case 'q':
      // send data
      if (send_data("q") < 0) {
        printf("ERROR: can't send data");
        return -1;
      }
      break;
    default:
      printf("opt: %d, optarg: %s\n", opt, optarg);
      print_help();
    }
  }

  return 0;
}

int main(int argc, char *argv[]) {
  // process command line arguments
  if (process_args(argc, argv) < 0) {
    exit(EXIT_FAILURE);
  }
  return 0;
}
