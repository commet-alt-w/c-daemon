# c-deamon

sample c-daemon project

a double fork unix daemon

## pre-requisites

- tools
  - gcc, cmake, make, valgrind

## building

- release
  - `$ make`
  - `$ make release`
- debug
  - `$ make debug`
- file watch
  - poll for file changes and build
  - release
    - `$ ./watch-src`
    - `$ ./watch-src release`
  - debug
    - `$ ./watch-src debug`
    
## running

- run daemon
  - `$ c-daemon qwer`
  - monitor daemon
    - `$ ps efj | grep c-daemon`
    - `$ tail -f /var/log/messages`
  - send sighup to daemon
    - `$ kill -1 $(pgrep c-daemon)`
  - send sigterm to daemon
    - `$ kill -15 $(pgrep c-daemon)`
- run client
    - `$ c-client -h`
  - message daemon
    - `$ c-client -b`
  - exit deamon using client
    - `$ c-client -q`

## memory leak tests

- valgrind
  - see `./check-for-memory-leaks.sh`
- exiting daemon started from valgrind
  - `$ kill -15 $(pgrep valgrind)`


